python-django-crispy-forms-foundation (1.0.2+ds-1) unstable; urgency=medium

  * [001fec7] d/watch: Add compression=gz
  * [e454ef1] New upstream version 1.0.2+ds
  * [eadf11f] d/copyright: Update year data
  * [090b4b3] d/control: Bump Standards-Version to 4.6.2
  * [e8d657a] d/control: Add BuildProfileSpecs
  * [21c8ccc] d/control: Add dh-sequence-sphinxdoc to B-D
  * [741755e] d/rules: Drop --with option from default target
  * [20a3703] d/control: Depend on python3-django-crispy-forms >= 2.0

 -- Carsten Schoenert <c.schoenert@t-online.de>  Sun, 25 Feb 2024 08:22:18 +0100

python-django-crispy-forms-foundation (0.9.0+ds-1) unstable; urgency=medium

  * [264d084] d/gbp.conf: More specific upstream data exclusion
    Do not exclude the complete sandbox folder, the content is needed for
    running the upstream test suite.
  * [c5b7854] New upstream version 0.9.0+ds
  * [a508f40] d/watch: Add repacksuffix to opts variable
  * [53f1372] d/copyright: Update year data, adjust base data
  * [e202f25] d/control: Bump Standards-Version to 4.6.1
    No further changes needed.
  * [934286d] d/{control,rules}: Move over to dh-sequence-python3
  * [72d354e] d/control: Add needed pytest packages to Build-Depends
  * [a8b4f38] d/rules: Run the internal tests while package build
  * [bd198bf] d/rules: Remove extra license file after install
  * [1f1fcfc] autopkgtest: Add first working setup

 -- Carsten Schoenert <c.schoenert@t-online.de>  Tue, 25 Oct 2022 19:28:56 +0200

python-django-crispy-forms-foundation (0.8.0+ds-1) unstable; urgency=medium

  * Upload to unstable
  * [472bfd4] d/control: Bump Standards-Version to 4.6.0
  * [27cafbe] d/control: Drop obsolete field Testsuite
  * [b58e991] d/control: Mark the -doc package Multi-Arch: foreign
  * [e78b96c] d/copyright: Drop Files-Excluded entry

 -- Carsten Schoenert <c.schoenert@t-online.de>  Sun, 26 Sep 2021 23:09:12 +0200

python-django-crispy-forms-foundation (0.8.0+ds-1~exp2) experimental; urgency=medium

  * [08f433d] d/control: Add python3-setuptools to Depends

 -- Carsten Schoenert <c.schoenert@t-online.de>  Sun, 30 May 2021 12:47:24 +0200

python-django-crispy-forms-foundation (0.8.0+ds-1~exp1) experimental; urgency=medium

  * [5065405] New upstream version 0.8.0+ds
  * [a8e67e4] First and basic Debianization (Closes: #988971)

 -- Carsten Schoenert <c.schoenert@t-online.de>  Sat, 22 May 2021 09:48:28 +0200
